import { Email } from "../model/email"

export const EMAILS_SENT: Email[] = [
    {
        id: '901',
        sender_email: '',
        sender_name: '',
        recipient_email: 'new@email.pl',
        recipient_name: 'XYZ',
        title: 'XYZ',
        body: ` Hi, <br>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Harum atque laudantium, quo minus culpa ex, eum ea, aperiam 
            tempore reiciendis error? Voluptatem dolor harum adipisci!
            <br><br> Best Regards
        `,
        date: new Date(Date.now()),
        visited: true,
    },
    {
        id: '902',
        sender_email: '',
        sender_name: '',
        recipient_email: 'Roksana@interia3.pl',
        recipient_name: 'Roksana',
        title: 'Email Project',
        body: ` Hi <br><br>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Harum atque laudantium, quo minus culpa ex, eum ea, aperiam tempore 
            eiciendis error? Voluptatem dolor harum adipisci!
            <br><br> Best Regards.
        `,
        date: new Date(Date.now()),
        visited: true,

    },
    {
        id: '903',
        sender_email: '',
        sender_name: '',
        recipient_email: 'Zbyszek@email.pl',
        recipient_name: 'Zbyszek',
        title: 'Invoice',
        body: `
            Hi, <br>
            <br> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iure 
            aliquam error quisquam. Dignissimos magni reiciendis facere alias neque
            ratione architecto? <br> <br> Best Regards
        `,
        date: new Date(Date.now()),
        visited: true,

    },
    {
        id: '904',
        sender_email: '',
        sender_name: '',
        recipient_email: 'darek@email.pl',
        recipient_name: 'Darek',
        title: 'Hi',
        body: `Hi, <br> Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Harum atque laudantium, quo minus culpa ex, eum ea, aperiam tempore 
            reiciendis error? Voluptatem dolor harum adipisci! <br>
            <br>
            Best Regards`,
        date: new Date(Date.parse("December 17, 1995 03:24:00")),
        visited: true,
        
    },
]