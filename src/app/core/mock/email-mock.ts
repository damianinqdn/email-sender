import { Email } from "./../model/email"

export const EMAILS: Email[] = [
    {
        id: '001',
        sender_email: '001@mail.pl',
        sender_name: 'Dawid Brzoza',
        recipient_email: '',
        recipient_name: '',
        title: 'Czesc Email testowy',
        body: 'lorem14 Body Email Section ',
        date: new Date(Date.now()),
        visited: false,
    },
    {
        id: '002',
        sender_email: '001@mail.pl',
        sender_name: 'Dawid Brzoza',
        recipient_email: '',
        recipient_name: '',
        title: 'Hi Email drugi',
        body: 'lorem14 Body Email Section Body Email Section ',
        date: new Date(Date.now()),
        visited: true,

    },
    {
        id: '003',
        sender_email: '001@mail.pl',
        sender_name: 'Dawid Brzoza',
        recipient_email: '',
        recipient_name: '',
        title: 'Deear Email trzeci',
        body: 'lorem14 Body Email Section Body Email Section Body Email Section ',
        date: new Date(Date.now()),
        visited: false,

    },
    {
        id: '004',
        sender_email: '001@mail.pl',
        sender_name: 'Dawid Brzoza',
        recipient_email: '',
        recipient_name: '',
        title: 'Title Email czwarty',
        body: `Hi, <br>
        Body Email Section Body Email Section Body Email Section Body Email Section ail Section Body Email Section
        ail Section Body Email Section ail Section Body Email Section <br>
        <br>
        Best Regards<br>
        <br>
        XYZ`,
        date: new Date(Date.now()),
        visited: false,
    }
]