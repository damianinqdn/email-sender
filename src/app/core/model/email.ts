export interface Email {
    id: string,
    sender_email: string,
    sender_name: string,
    recipient_email: string,
    recipient_name: string,
    title: string,
    body: string,
    date: Date,
    visited: boolean,
}
