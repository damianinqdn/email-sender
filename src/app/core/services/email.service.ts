import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { EMAILS } from '../mock/email-mock';
import { Email } from '../model/email';

@Injectable({
  providedIn: 'root'
})
export class EmailService {
  
  
  private emails = EMAILS
  
  constructor() {
    this.getEmails();
  }

  getEmails() {
    return of(this.emails)
  }

  getEmailById(id: string) {
    // return of(this.emails.find(e => e.id == id))
    const email = this.emails.find(e => e.id == id);
    return of(email);
  }

  setVisited(email: Email) {
    this.emails.forEach(e => {
      if(e.id == email.id) {
        e.visited = true;
      }
    });
  }
  
}
