import { TestBed } from '@angular/core/testing';

import { EmailSentService } from './email-sent.service';

describe('EmailSentService', () => {
  let service: EmailSentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmailSentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
