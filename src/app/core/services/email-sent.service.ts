import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { EMAILS_SENT } from '../mock/email-sent-mock';
import { Email } from '../model/email';


@Injectable({
  providedIn: 'root'
})
export class EmailSentService {

  private emails = EMAILS_SENT

  // constructor() {
  //   this.getMySentEmails();
  //  }

  getMySentEmails() {
    return of(this.emails);
  }

  getMySentEmailById(id: string) {
    const email = this.emails.find(e => e.id == id);
    return of(email);
  }

  sendEmail(e: any) {
    const newEmail:Email = {
      id: '996',
      sender_email: 'my@mail.com',
      sender_name: 'Damian',
      recipient_email: e.recipient,
      recipient_name: '',
      title: e.title,
      body: e.body,
      date: new Date(Date.now()),
      visited: true,
    }
    this.emails.push(newEmail);
  }
}
