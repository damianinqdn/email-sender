import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmailSentService } from 'src/app/core/services/email-sent.service';

@Component({
  selector: 'app-email-create',
  templateUrl: './email-create.component.html',
  styleUrls: ['./email-create.component.css']
})
export class EmailCreateComponent implements OnInit {

  constructor(private emailSentService:EmailSentService) { }

  ngOnInit(): void {
  }

  emailForm = new FormGroup({
    recipient: new FormControl('', [Validators.email, Validators.required]),
    title: new FormControl('', [Validators.required]),
    body: new FormControl(''),
  });

  sendEmail() {
    this.emailSentService.sendEmail(this.emailForm.value);
    console.log("Email was sent", this.emailForm.value);
    this.emailForm.reset();
  }

}
