import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Email } from 'src/app/core/model/email';
import { EmailService } from 'src/app/core/services/email.service';

@Component({
  selector: 'app-emails-list',
  templateUrl: './emails-list.component.html',
  styleUrls: ['./emails-list.component.css']
})
export class EmailsListComponent implements OnInit {

  emails: Observable<Email[]> | undefined;

  selected: Email | undefined;

  constructor(private emailService: EmailService) { }

  ngOnInit(): void {
    this.emails = this.emailService.getEmails();
  }

  selectOne(email:Email) {
    this.selected = email;
  }

}
