import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Email } from 'src/app/core/model/email';
import { EmailSentService } from 'src/app/core/services/email-sent.service';

@Component({
  selector: 'app-email-sent-details',
  templateUrl: './email-sent-details.component.html',
  styleUrls: ['./email-sent-details.component.css']
})
export class EmailSentDetailsComponent implements OnInit {

  email:Email | undefined;

  constructor(private emailService: EmailSentService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getEmail();
  }

  getEmail() :void {
    const id = this.route.snapshot.paramMap.get('id');
    this.emailService.getMySentEmailById(''+id)
      .subscribe(email => this.email = email);
  }

}
