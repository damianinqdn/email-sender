import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSentDetailsComponent } from './email-sent-details.component';

describe('EmailSentDetailsComponent', () => {
  let component: EmailSentDetailsComponent;
  let fixture: ComponentFixture<EmailSentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailSentDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
