import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailCreateComponent } from './email-create/email-create.component';
import { EmailDetailsComponent } from './email-details/email-details.component';
import { EmailSentDetailsComponent } from './email-sent-details/email-sent-details.component';
import { EmailComponent } from './email.component';
import { EmailsListComponent } from './emails-list/emails-list.component';
import { EmailsSentComponent } from './emails-sent/emails-sent.component';

const routes: Routes = [{
  path: '',
  component: EmailComponent,
  children: [
    {
      path: 'received',
      component: EmailsListComponent
    },
    {
      path: 'sent',
      component: EmailsSentComponent
    },
    {
      path: 'received/:id',
      component: EmailDetailsComponent
    },
    {
      path: 'sent/:id',
      component: EmailSentDetailsComponent
    },
    {
      path: 'create',
      component: EmailCreateComponent
    },
  ]
  
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailRoutingModule { }
