import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailsSentComponent } from './emails-sent.component';

describe('EmailsSentComponent', () => {
  let component: EmailsSentComponent;
  let fixture: ComponentFixture<EmailsSentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailsSentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailsSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
