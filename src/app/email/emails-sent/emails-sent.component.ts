import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Email } from 'src/app/core/model/email';
import { EmailSentService } from 'src/app/core/services/email-sent.service';

@Component({
  selector: 'app-emails-sent',
  templateUrl: './emails-sent.component.html',
  styleUrls: ['./emails-sent.component.css']
})
export class EmailsSentComponent implements OnInit {

  emails: Observable<Email[]> | undefined;

  constructor(private emailSentService: EmailSentService) { }

  ngOnInit(): void {
    this.emails = this.emailSentService.getMySentEmails();
  }
}
