import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Email } from 'src/app/core/model/email';
import { EmailService } from 'src/app/core/services/email.service';

@Component({
  selector: 'app-email-details',
  templateUrl: './email-details.component.html',
  styleUrls: ['./email-details.component.css']
})
export class EmailDetailsComponent implements OnInit {

  email: Email | undefined;

  constructor(private emailService: EmailService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getEmail();
  }

  getEmail(): void {
    const id = (this.route.snapshot.paramMap.get('id'));
    this.emailService.getEmailById(''+id)
      .subscribe(email => this.email = email);
    this.emailService.setVisited(this.email!);
  }

}
