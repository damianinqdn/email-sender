import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmailRoutingModule } from './email-routing.module';
import { EmailsListComponent } from './emails-list/emails-list.component';
import { EmailComponent } from './email.component';
import { EmailDetailsComponent } from './email-details/email-details.component';
import { EmailsSentComponent } from './emails-sent/emails-sent.component';
import { EmailSentDetailsComponent } from './email-sent-details/email-sent-details.component';
import { EmailCreateComponent } from './email-create/email-create.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    EmailsListComponent,
    EmailComponent,
    EmailDetailsComponent,
    EmailsSentComponent,
    EmailSentDetailsComponent,
    EmailCreateComponent,
  ],
  imports: [
    CommonModule,
    EmailRoutingModule,
    ReactiveFormsModule,
  ]
})
export class EmailModule { }
